def checkRaidDrives(controller):
    if controller == "MegaRAID":
        mega_general = runCommand(['/usr/local/bin/megacli', '-ldinfo', '-lall', '-a0' ])
        mega_battery = runCommand(['/usr/local/bin/megacli', '-AdpBbuCmd', '-GetBbuStatus', '-a0'])
        
        if mega_general:
            raid_level = re.findall(r'^RAID\sLevel\s+:\s([\w\d\s,-]+)$', mega_general, re.MULTILINE+re.DOTALL)
            raid_size = re.findall(r'^Size\s+:\s([\d\.]+\s[\w]{2})', mega_general, re.MULTILINE+re.DOTALL)
            raid_state = re.findall(r'^State\s+:\s([\w]+)$', mega_general, re.MULTILINE+re.DOTALL)
            raid_strip_size = re.findall(r'^Strip\sSize\s+:\s([\d\w\s]+)$', mega_general, re.MULTILINE+re.DOTALL)
            if mega_battery:
                battery_state = re.findall(r'^Battery\sState\s+:\s([\w\d\s]+)$', mega_battery, re.MULTILINE+re.DOTALL)
                charging_status = re.findall(r'^\s+Charging\sStatus\s+:\s([\w\d\s]+)$', mega_battery, re.MULTILINE+re.DOTALL)
                charge_percent = re.findall(r'^\s+Relative\sState\sof\sCharge:\s([\w\s\d%]+)$', mega_battery, re.MULTILINE+re.DOTALL)
                cache_settings = re.findall(r'^Current\sCache\sPolicy:\s([\w\s,]+)$', mega_general, re.MULTILINE+re.DOTALL)
            
                if battery_state:
                    print "Battery State: %s" % battery_state[0].strip()
                    print "Charging Status: %s" % charging_status[0]
                    print "Charge Level: %s" % charge_percent[0]
                    wbb = runCommand(['/usr/local/bin/megacli', '-LDSetProp', 'WB', '-LALL', '-aALL']) # Enable BBU Write Cache
                    runCommand(['/usr/local/bin/megacli', '-LDSetProp', 'NoCachedBadBBU', '-LALL', '-aALL']) # Also disable write cache if BBU fails
                    print "Current Cache Policy: %s" % cache_settings[0]
            print "------------------------"

            # If Server contains a raid and a backup drive -- print info for both
            if len(raid_level) == 2:
                for i in range(2):
                    print "Raid Level: %s" % raid_level[i]
                    print "Size: %s" % raid_size[i]
                    print "State: %s" % raid_state[i]
                    print "Strip Size: %s" % raid_strip_size[i]
                    print "------------------------"
                  
            # If Server has 1 raid
            elif len(raid_level) == 1:
                print "Raid Level: %s" % raid_level[0]
                print "Size: %s" % raid_size[0]
                print "State: %s" % raid_state[0]
                print "Strip Size: %s" % raid_strip_size[0]
                print "------------------------" 

            #-------------------------------------------
            # Check individual drives in array 
            #-------------------------------------------
            # -pdlist -a0
            mega_drives = runCommand(['/usr/local/bin/megacli', '-pdlist', '-a0'])
            port_statuses = re.findall(r'^Firmware\sstate:\s([\w\s(),]+)$', mega_drives, re.MULTILINE+re.DOTALL)
            for port in range(len(port_statuses)):
                print "Port %d is %s" % (port, port_statuses[port])
            print "------------------------" 

            print ""
        
    elif controller == "3ware":
        # Add check in for /root/3ware/tw_cli binary -- Need to get correct path
        if os.path.exists('/usr/local/bin/3ware'):
            threeware = '/usr/local/bin/3ware'
        else:
            threeware = None
        if threeware:
            subprocess.call([threeware, '/c0', 'show'])

    elif controller == "Adaptec":
        adaptec_general = runCommand(['/usr/local/bin/arcconf', 'getconfig', '1'])
        if adaptec_general:
            controller_model = re.findall(r'^\s+Controller\sModel\s+:\s([\w\s\d]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            firmware_version = re.findall(r'^\s+Firmware\s+:\s([\w\d\.\-\s()]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_level = re.findall(r'^\s+RAID\slevel\s+:\s([\d\w]+)', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_size = re.findall(r'^\s+Size\s+: ([\d\s\w]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_state = re.findall(r'^\s+Status\sof\slogical\sdevice\s+:\s(\w+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_bbu_status = re.findall(r'^\s+Status\s+:\s(Charging|Optimal)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_bbu_capacity = re.findall(r'^\s+Capacity\sremaining\s+:\s(\d{1,3}\spercent)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_bbu_time_Remaining = re.findall(r'^\s+Time\sremaining\s\(at\scurrent\sdraw\)\s+:\s([\d\s\w,]+)$', adaptec_general, re.MULTILINE+re.DOTALL) # use a .strip()
            raid_cache_setting = re.findall(r'^\s+Write-cache\ssetting\s+:\s([\w\s()-\/]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            
            # Check BBU
            if raid_bbu_status:
                print "BBU Status: %s" % raid_bbu_status[0]
                if raid_bbu_capacity:
                    print "Capacity Remaining: %s" % raid_bbu_capacity[0]
                if raid_bbu_time_Remaining:
                    print "Time Remaing (at current draw): %s" % raid_bbu_time_Remaining[0].strip()
                wbb = runCommand(['/usr/local/bin/arcconf', 'setcache', '1', 'logicaldrive', '0', 'wbb'])
                if wbb:
                    print "Write Back Cache has been enabled. Please re-run script to confirm."
                if raid_cache_setting:
                    print "Write-cache Setting: %s" % raid_cache_setting[0]
            print "------------------------"

            # Output if 1 raid
            if len(raid_level) == 1:
                print "Controller Model: %s" % controller_model[0]
                print "Firmware Version: %s" % firmware_version[0]
                print "Raid Level: %s" % raid_level[0]
                print "Raid Size: %s" % raid_size[0]
                print "Raid State: %s" % raid_state[0]
                print "------------------------"

            # Output if raid  with a backup drive
            elif len(raid_level) == 2:
                print "Controller Model: %s" % controller_model[0]
                print "Firmware Version: %s" % firmware_version[0]
                for i in range(2):
                    print "Raid Level: %s" % raid_level[i]
                    print "Raid Size: %s" % raid_size[i]
                    print "Raid State: %s" % raid_state[i]
                    print "------------------------"
    
            #-------------------------------------------
            # Check individual drives in Adaptec array 
            #-------------------------------------------
            device_state = re.findall(r'^\s+State\s+:\s([\w\s]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            for device in range(len(device_state)):
                print "Port %d is %s" % (device, device_state[device])
            print ""