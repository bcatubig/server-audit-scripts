#!/usr/bin/env python
# IPMI Install Script
# Author: Brandon Catubig
# Email: brandon.c@hostdime.com
import os, sys, subprocess, re, time, platform

class bcolors:
    FAIL = '\033[0;31m' # RED
    SUCCESS = '\033[0;32m' # GREEN
    ENDC = '\033[0m' # Default

    def disable(self):
        self.FAIL = ''
        self.ENDC = ''
        self.SUCCESS = ''

def runCommand(cmd):
    '''
    Takes in a list which should include the unix binary absolute path and optional parameters
    Returns: String output from command or None if there was an error.
    '''
    try:
        out = None
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out = p.communicate()
    except:
        return None
    if p.returncode == 0:
        return out[0]
    else:
        return None

def menu():
    sys.stderr.write("\x1b[2J\x1b[H")
    print """
  ___            _   ___      
 |_ _|_ __ _ __ (_) | _ \_  _ 
  | || '_ \ '  \| | |  _/ || |
 |___| .__/_|_|_|_| |_|  \_, |
     |_|                 |__/ 
    """
    print "Hello. Welcome to the IPMI tool. Please select an option"
    print "--------------------------------------------"
    print "1. System Information"
    print "2. Clear current IPMI configuration"
    print "3. Configure IPMI (Standard Hostdime Config)"
    print "4. Set LAN Mode"
    print "5. View/Add user accounts"
    print "--------------------------------------------"

    #-------------------------------------------
    # Get user input
    #-------------------------------------------
    choice = ""
    while (type(choice) is str):
        try:
            choice = int(raw_input("> "))
        except KeyboardInterrupt:
            sys.exit()
        except:
            print "You entered invalid input. Please try again"
    return choice

def loadModules():
    #-------------------------------------------
    # Load IPMI Modules
    #-------------------------------------------
    kernel = platform.uname()[2]
    location = '/lib/modules/' + kernel + '/kernel/drivers/char/ipmi/'
    for module in os.listdir(location):
        match = re.search("(ipmi_\w+)\.ko", module)
        runCommand(['/sbin/modprobe', match.groups()[0]])
    print bcolors.SUCCESS + "[+] Modules Loaded!" + bcolors.ENDC

def installIPMI_TOOLS():
    #-------------------------------------------
    # Check to see if ipmitool is installed
    #-------------------------------------------
    sys.stderr.write("\x1b[2J\x1b[H")
    print "Checking to see if ipmitool is installed...",
    if os.path.exists('/usr/bin/ipmitool'):
        print "Yes it is!"
    else:
        print "Nope!"
        if os.path.exists('/usr/bin/yum'): # CentOS
            packageManager = '/usr/bin/yum'
            ipmi_package = 'OpenIPMI-tools'
        elif os.path.exists('/usr/bin/apt-get'): # Debian
            packageManager = '/usr/bin/apt-get'
            ipmi_package = 'ipmitool'
        else:
            print bcolors.FAIL + "I can't find a package manager to use! Is this GNU/Linux?!" + bcolors.ENDC
            sys.exit(1)
        
        #-------------------------------------------
        # The actual install process
        #-------------------------------------------
        print "Installing packages...please wait..."
        run_install = runCommand([packageManager, '-y', 'install', ipmi_package])
        if run_install:
            print bcolors.SUCCESS + "[+] ipmitool was installed successfully!" + bcolors.ENDC
            raw_input("Press enter to continue...")
        else:
            print bcolors.FAIL + "[-] Error: Could not install ipmitools (Are you running as root?)" + bcolors.ENDC
            sys.exit(1)

def check_IPMI():
    ipmi = runCommand(['/usr/bin/ipmitool', 'lan', 'print', '1'])
    if ipmi:
        mac = re.search(r'^MAC\s\w+\s+:\s([\d\w]{2}:[\d\w]{2}:[\d\w]{2}:[\d\w]{2}:[\d\w]{2}:[\d\w]{2})$', ipmi, re.MULTILINE+re.DOTALL)
        if mac:
            print "IPMI is detected"
    else:
        print bcolors.FAIL + "A valid IPMI device is not detected on the system. Program terminating." + bcolors.ENDC
        sys.exit(1)

def main():
    installIPMI_TOOLS()
    loadModules()
    check_IPMI()
    bios_vendor = ""
    motherboard_manufacturer = ""
    motherboard_model = ""
    system_info = runCommand(['/usr/sbin/dmidecode'])
    ipmi_info = runCommand(['/usr/bin/ipmitool', 'lan', 'print', '1'])

    while(True):
        choice = menu()
        system_info = runCommand(['/usr/sbin/dmidecode'])
        ipmi_info = runCommand(['/usr/bin/ipmitool', 'lan', 'print', '1'])
        
        #-------------------------------------------
        # HAVE IT YOUR WAY.
        #-------------------------------------------
        if choice == 1:
            
            bios_vendor = re.findall(r'Vendor:\s([\w\s\.]+)$', system_info, re.MULTILINE+re.DOTALL)
            motherboard_manufacturer = re.findall(r'Manufacturer:\s([\d\w\s-]+)$', system_info, re.MULTILINE+re.DOTALL)
            motherboard_model = re.findall(r'Product\sName:\s([\w\d\/]+)', system_info, re.MULTILINE+re.DOTALL)

            if bios_vendor:
                print "Bios Vendor: %s" % bios_vendor[0]
            if motherboard_manufacturer:
                print "Motherboard: %s" % motherboard_manufacturer[1]
            if motherboard_model:
                if motherboard_model[0] != "empty":
                    print "Motherboard Model: %s" % motherboard_model[0]
                else:
                    print "Motherboard Model: %s" % motherboard_model[1]

            # ----------------------------------------------------------
            # Current IPMI Config
            # ----------------------------------------------------------
            if ipmi_info:
                source = re.findall(r'^IP\sAddress\sSource\s+:\s([\w\d\s]+)$', ipmi_info, re.MULTILINE+re.DOTALL)
                ip = re.findall(r'^IP\sAddress\s+:\s(\b(?:\d{1,3}\.){3}\d{1,3}\b)$', ipmi_info, re.MULTILINE+re.DOTALL)
                subnet = re.findall(r'^Subnet\sMask\s+:\s(\b(?:\d{1,3}\.){3}\d{1,3}\b)$', ipmi_info, re.MULTILINE+re.DOTALL)
                gateway = re.findall(r'^Default\sGateway\sIP\s+:\s(\b(?:\d{1,3}\.){3}\d{1,3}\b)$', ipmi_info, re.MULTILINE+re.DOTALL)
                print "--------------------------------"
                if source:
                    print "Source: %s" % source[0]
                if ip:
                    print "IPMI IP: %s" % ip[0]
                if subnet:
                    print "Subnet Mask: %s" % subnet[0]
                if gateway:
                    print "Gateway: %s" % gateway[0]


            raw_input("Press enter to continue...")
        
        #---------------------------------------------------------------------------------------
        # Clear Current IPMI config 
        elif choice == 2:
            runCommand(['/usr/bin/ipmitool', 'lan', 'set', '1', 'ipaddr', '0.0.0.0'])
            runCommand(['/usr/bin/ipmitool', 'lan', 'set', '1', 'netmask', '0.0.0.0'])
            runCommand(['/usr/bin/ipmitool', 'lan', 'set', '1', 'defgw', 'ipaddr', '0.0.0.0'])
            print bcolors.SUCCESS + "Cleared IP, Netmask, and Gateway" + bcolors.ENDC
            raw_input("Press enter to continue...")
        
        #---------------------------------------------------------------------------------------
        # Install defaults hostdime IPMI configuration 
        elif choice == 3:
            try:
                #-------------------------------------------
                # Get info from users
                #-------------------------------------------
                ip = raw_input("What is the IPMI ip?: ")
                netmask = raw_input("What is the netmask?: ")
                gateway = raw_input("What is the gateway?: ")
                print "--------------------------------"
                hdroot_pw = raw_input("What is the hdroot password?: ")
                hdview_pw = raw_input("What is the hdview password?: ")

                print "Setting IPMI info. Please wait..."
                #-------------------------------------------
                # Set IPMI
                #-------------------------------------------
                runCommand(['/usr/bin/ipmitool', 'lan', 'set', '1', 'ipsrc', 'static'])
                runCommand(['/usr/bin/ipmitool', 'lan', 'set', '1', 'ipaddr', ip.strip()])
                runCommand(['/usr/bin/ipmitool', 'lan', 'set', '1', 'netmask', netmask.strip()])
                runCommand(['/usr/bin/ipmitool', 'lan', 'set', '1', 'defgw', 'ipaddr', gateway.strip()])
                #-------------------------------------------
                # Set Users
                #-------------------------------------------
                runCommand(['/usr/bin/ipmitool', 'user', 'set', 'name', '9', 'hdview'])
                runCommand(['/usr/bin/ipmitool', 'user', 'set', 'password', '9', hdview_pw.strip()])
                runCommand(['/usr/bin/ipmitool', 'channel', 'setaccess', '1', '9', 'ipmi=on', 'privilege=3', 'link=on', 'callin=on'])
                runCommand(['/usr/bin/ipmitool', 'user', 'enable', '9'])
                
                
                runCommand(['/usr/bin/ipmitool', 'user', 'set', 'name', '10', 'hdroot'])
                runCommand(['/usr/bin/ipmitool', 'user', 'set', 'password', '10', hdroot_pw.strip()])
                runCommand(['/usr/bin/ipmitool', 'channel', 'setaccess', '1', '10', 'ipmi=on', 'privilege=4', 'link=on', 'callin=on'])
                runCommand(['/usr/bin/ipmitool', 'user', 'enable', '10'])
                
                print bcolors.SUCCESS + "[+] IPMI has been installed!" + bcolors.ENDC
                raw_input("Press enter to continue...")
            except KeyboardInterrupt:
                continue
        
        #---------------------------------------------------------------------------------------    
        # Change IPMI Lan mode 
        elif choice == 4:
            # http://www.supermicro.com/support/faqs/faq.cfm?faq=15868
            # Send RAW commands
            # Supermicro / TYAN
            # SM Models: X9SRE/X9SRE-3F/X9SRi/X9SRi-3F, X9SCL/X9SCM, X8DTL
            # ipmitool raw 0x30 0x70 0x0c 1 1 
            bios_vendor = re.findall(r'Vendor:\s([\w\s\.]+)$', system_info, re.MULTILINE+re.DOTALL)
            motherboard_manufacturer = re.findall(r'Manufacturer:\s([\d\w\s-]+)$', system_info, re.MULTILINE+re.DOTALL)
            motherboard_model = re.findall(r'Product\sName:\s([\w\d\/]+)', system_info, re.MULTILINE+re.DOTALL)
            
            if motherboard_manufacturer == "TYAN":
                print bcolors.FAIL + "TYAN Features not yet implemented." + bcolors.ENDC
                raw_input("Press enter to continue...")
                continue

            if bios_vendor:
                if 'American' in bios_vendor[0]:
                    bios = 'ami'
                    print "Server is using AMI bios"
                else:
                    bios = 'aten'
                    print 'Server is using ATEN/KVM'
                    raw_input("Press enter to continue...")
                    continue
            
            if motherboard_model:
                if 'X8' in motherboard_model[0]:
                    model = 'X8'
                    print "Model: X8"
                elif 'X9' in motherboard_model[0]:
                    model = 'X9'
                    print "Model: X9"
            
            print "Please choose a LAN mode"
            print "1. Dedicated - Always use dedicated IPMI device"
            print "2. Shared (recommended) - Always use the LAN1 interface"
            print "3. Failover (default - not recommended) - On boot, detect if the dedicated IPMI interface is connected.\nIf so, use the dedicated interface, otherwise fall back to the shared LAN1. "
            
            try:
                lan_choice = raw_input("> ")

                if bios == 'ami' and model == 'X8':
                    if lan_choice == '1':
                        # Dedicated - 0x30 0x70 0x0c 1 1 0 
                        runCommand(['/usr/bin/ipmitool', 'raw', '0x30', '0x70', '0x0c', '1', '1', '0' ])
                        print bcolors.SUCCESS + "Lan mode set to DEDICATED." + bcolors.ENDC
                    elif lan_choice == '2':
                        # Shared - 0x30 0x70 0x0c 1 1 1 
                        runCommand(['/usr/bin/ipmitool', 'raw', '0x30', '0x70', '0x0c', '1', '1', '1' ])
                        print bcolors.SUCCESS + "Lan mode set to SHARED." + bcolors.ENDC
                    elif lan_choice == '3':
                        # Failover - 0x30 0x70 0x0c 1 0 
                        runCommand(['/usr/bin/ipmitool', 'raw', '0x30', '0x70', '0x0c', '1', '0'])
                        print bcolors.SUCCESS + "Lan mode set to FAILOVER." + bcolors.ENDC

                if bios == 'ami' and model == 'X9':
                    if lan_choice == '1':
                        # Dedicated - 0x30 0x70 0x0c 1 0  
                        runCommand(['/usr/bin/ipmitool', 'raw', '0x30', '0x70', '0x0c', '1', '0'])
                        print bcolors.SUCCESS + "Lan mode set to DEDICATED." + bcolors.ENDC
                    elif lan_choice == '2':
                        # Shared - 0x30 0x70 0x0c 1 1 
                        runCommand(['/usr/bin/ipmitool', 'raw', '0x30', '0x70', '0x0c', '1', '1'])
                        print bcolors.SUCCESS + "Lan mode set to SHARED." + bcolors.ENDC
                    elif lan_choice == '3':
                        # Failover - 0x30 0x70 0x0c 1 2 
                        runCommand(['/usr/bin/ipmitool', 'raw', '0x30', '0x70', '0x0c', '1', '2'])
                        print bcolors.SUCCESS + "Lan mode set to FAILOVER." + bcolors.ENDC


                raw_input("Press enter to continue...")
            
            except KeyboardInterrupt:
                continue
        
        #---------------------------------------------------------------------------------------
        
        elif choice == 5:
            # Create users
            # Find out max users - ipmitool channel getaccess 1
            #-----------------------------------------------------------------------------------------
            # See which accounts in use (Usually 2,9,10 - with 10 accounts supported max )
            # Find out what accounts are in use & print users - ipmitool user list - (\d{1,2})\s+(\w+)
            print "Current IPMI users"
            print "--------------------------------"
            subprocess.call(['/usr/bin/ipmitool', 'user', 'list', '1'])
            print "--------------------------------"
            create_new = raw_input("Would you like to create a new user account? (y/n): ")
            if create_new.lower() == "n":
                continue
            else:
                user_id = raw_input('Enter a user account ID: ')
                user_name = raw_input('Enter a user account name: ')
                user_password = raw_input('Enter the user password: ')
                user_access_level = raw_input('Enter the user access level (2 (User), 3 (Operator), 4 (Admin)): ')

                runCommand(['/usr/bin/ipmitool', 'user', 'set', 'name', user_id, user_name.strip()])
                runCommand(['/usr/bin/ipmitool', 'user', 'set', 'password', user_id, user_password.strip()])
                runCommand(['/usr/bin/ipmitool', 'channel', 'setaccess', '1', user_id, 'ipmi=on', 'privilege='+user_access_level, 'link=on', 'callin=on'])
                runCommand(['/usr/bin/ipmitool', 'user', 'enable', user_id])

                # Print out user list & permissions to verify that user was created successfully
                print bcolors.SUCCESS + '[+] User %s was created successfully!' % user_name + bcolors.ENDC
                print "--------------------------------"
                subprocess.call(['/usr/bin/ipmitool', 'user', 'list', '1'])

                raw_input("Press enter to continue...")

if __name__ == "__main__":
    main()
