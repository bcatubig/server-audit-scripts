#!/usr/bin/env python
# XIAAS (XIAAS is another audit script)
# Maintained by Brandon Catubig
# Email: brandon.c@hostdime.com
# Version: Release

import re, subprocess, sys, os, platform, time

# Color Class for print output
class bcolors:
    FAIL = '\033[0;31m' # RED
    SUCCESS = '\033[0;32m' # GREEN
    ENDC = '\033[0m' # Default

    def disable(self):
        self.FAIL = ''
        self.ENDC = ''
        self.SUCCESS = ''

# General Methods
def runCommand(cmd):
    '''
    Takes in a list which should include the unix binary absolute path and optional parameters
    Returns: String output from command or None if there was an error.
    '''
    try:
        out = None
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = p.communicate()
    except:
        return None

    if p.returncode == 0:
        return output[0]
    else:
        return None

def openFile(input):
    """
    Takes in a location as a string and attempts to open the file
    Returns: String output from file if successful or None if there is an exception
    """
    try:
        f = open(input, 'r')
        result = f.read()
        f.close()
        return result
    except:
        print bcolors.FAIL + "[-] Error: Could not open the file!" + bcolors.ENDC
        return None

def parseCPU(cpu_input):
    """
    Parses input from /proc/cpuinfo for the CPU Model
    """
    # ^model\sname\s+:\s?([\w\s@\d\.()]+)$
    # ^model\sname\s+:\s([\w\d\s-@\.()]+)$
    #result = re.findall(r'model\sname\s+:\s+([\w\d\s()-]+)$|model\sname\s+:\s+([\w\d\s()-]+@\s\d\.\d{1,3}\w{3})|^model\sname\s+:\s([\w\d\s()\.]+)$', cpu_input, re.MULTILINE+re.DOTALL)
    result = re.findall(r'^model\sname\s+:\s(?P<model>[\w\d\s\-@\.()\+]+)$', cpu_input, re.MULTILINE+re.DOTALL)
    if result:
        # for cpu in result.groups():
        #     if cpu is not None:
        #         return cpu
        return result[0]
    else:
        return None

def checkBackupConfig():
    if os.path.exists('/etc/cpbackup.conf'):
        cpanel_file = openFile('/etc/cpbackup.conf')
        
        #findall because I am a winner
        enabled = re.findall(r'^BACKUPENABLE\s(yes)$', cpanel_file, re.MULTILINE+re.DOTALL)
        backup_int = re.findall(r'^BACKUPINT\s(daily|weekly|monthly)$', cpanel_file, re.MULTILINE+re.DOTALL)
        backup_accounts = re.findall(r'^BACKUPACCTS\s(yes)$', cpanel_file, re.MULTILINE+re.DOTALL)
        backup_days = re.findall(r'^BACKUPDAYS\s(0,1,2,3,4,5,6)$', cpanel_file, re.MULTILINE+re.DOTALL)
        mysql_backup = re.findall(r'^MYSQLBACKUP\s(both)$', cpanel_file, re.MULTILINE+re.DOTALL)
        backup_inc = re.findall(r'^BACKUPINC\s(yes)$', cpanel_file, re.MULTILINE+re.DOTALL)
        backup_ret_weekly = re.findall(r'^BACKUPRETWEEKLY\s(\d)$', cpanel_file, re.MULTILINE+re.DOTALL)
        backup_ret_daily = re.findall(r'^BACKUPRETDAILY\s(\d)$', cpanel_file, re.MULTILINE+re.DOTALL)
        backup_ret_monthly = re.findall(r'^BACKUPRETMONTHLY\s(\d)$', cpanel_file, re.MULTILINE+re.DOTALL)

        
        # Check if enabled
        if enabled:
            if enabled[0] == "yes":
                print bcolors.SUCCESS + "Backups are enabled via Cpanel" + bcolors.ENDC
        else:
            print bcolors.FAIL + "Backups are NOT enabled for Cpanel!" + bcolors.ENDC
        
        print "Backups are set to retain:",
        if backup_ret_daily:
            if int(backup_ret_daily[0]) == 1:
                print "Daily",

        if backup_ret_weekly:
            if int(backup_ret_weekly[0]) == 1:
                print "Weekly",
        
        if backup_ret_monthly:
            if int(backup_ret_monthly[0]) == 1:
                print "Monthly",

        print ""

        # Check that individual accounts are enabled for backup
        if backup_accounts:
            if backup_accounts[0] == "yes":
                print "Backups for individual accounts are enabled"
        else:
            print bcolors.FAIL + "!! Backups for individual accounts are NOT enabled !!" + bcolors.ENDC
        
        # Check incremental backups
        if backup_inc:
            if backup_inc[0] == "yes":
                print "Backups are set to: Incremental"
        else:
            print bcolors.FAIL + "!! Incremental backups are NOT enabled !!" + bcolors.ENDC

        # Check if configured for daily interval backups
        if backup_int:
            if backup_int[0] == "daily":
                print "Backups are set to run: DAILY"
            elif backup_int[0] == "weekly":
                print "Backups are set to run: WEEKLY"
            elif backup_int[0] == "monthly":
                print "Backups are set to run: MONTHLY"

        # Are backups running daily?
        if backup_days:
            if backup_days[0] == "0,1,2,3,4,5,6":
                print "Backups are scheduled to run everyday - Monday -> Sunday"
        else:
            print bcolors.FAIL + "!! Backups are NOT set to run everyday !!" + bcolors.ENDC

        # Make sure cpanel is backing up mysql for users AND the mysql directory
        if mysql_backup:
            if mysql_backup[0] == "both":
                print "MySQL backup is Per Account and Entire MySQL Directory\n"
        else:
            print bcolors.FAIL + "!! MySQL is not configured to backup accounts and the MySQL directory !!\n" + bcolors.ENDC

    else:
        print bcolors.FAIL + "[-] Error: The cpanel backup file could not be opened!\n" + bcolors.ENDC

def parseEthSpeed(eth):
    """
    Takes in input from ethtool and returns the eth speed
    """
    result = re.search(r"Speed:\s+([\d]{2,4}\w+/s)$", eth, re.MULTILINE+re.DOTALL)
    if result:
        return result.groups()[0]
    else:
        return None

def checkOSRelease():
    """
    Checks to see if server is running CentOS / Debian / Ubuntu
    """
    if os.path.exists('/etc/debian_version'):
        deb_version = openFile('/etc/debian_version')
        return "Debian: " + deb_version
    elif os.path.exists('/etc/redhat-release'):
        redhat_version = openFile('/etc/redhat-release')
        return redhat_version
    else:
        print bcolors.FAIL + "[-] Error: Was not able to determine the OS Distro." + bcolors.ENDC
        return None

def parseEthMacs(eth_macs, eth_dev):
    """
    Takes in input from 'ifconfig -a' and searches for all available mac address
    Returns a list with all found MAC addresses or None
    """
    # Identify if primary eth device is eth0 or eth1
    # Return first mac result of default eth device
    result = re.findall('HWaddr\s([\d\w]{2}:[\d\w]{2}:[\d\w]{2}:[\d\w]{2}:[\d\w]{2}:[\d\w]{2})', eth_macs, re.MULTILINE+re.DOTALL)
    if result:
        return result[0]
    else:
        return None

def checkRaidCard():
    """
    This Method will parse lspci to determine if a raid card is installed on the system.
    Returns a string if a raid card is detected or None if none are found
    """
    # This hack checks for the correct location of lspci
    # Its different on CentOS and Debian
    if os.path.exists('/sbin/lspci'):
        lspci = '/sbin/lspci'
    else:
        lspci = '/usr/bin/lspci'

    info = runCommand([lspci, '-mm'])
    if not info:
        return False
    result = re.findall(r"(MegaRAID|Adaptec|3ware)", info, re.IGNORECASE+re.MULTILINE+re.DOTALL)
    if result:
        return result[0]
    else:
        return None

def getIPS():
    ipaddr = runCommand(['/sbin/ip', 'addr'])
    result = re.findall(r'inet\s(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', ipaddr, re.MULTILINE+re.DOTALL)
    if result:
        return result
    else:
        return None

def testPing(ip):
    ping = runCommand(['/bin/ping', '-I', ip, '4.2.2.1', '-c1', '-w 5'])
    return ping

def checkService(service_name):
    # ['Axon', 'Apache', 'MySQL', 'Named']
    if service_name == 'Axon':
        result = runCommand(['/etc/init.d/axond', 'status'])
        if result and re.findall(r"(Axon is running)", result, re.MULTILINE+re.DOTALL):
                return True
        else:
            return False            
    
    elif service_name == 'Apache':
        result = runCommand(['/bin/ps', 'aux'])
        if result and re.findall(r"(httpd|nginx)", result, re.MULTILINE+re.DOTALL):
            return True
        else:
            return False
    elif service_name == 'MySQL':
        result = runCommand(['/bin/ps', 'aux'])
        if result and re.findall(r'(mysqld)', result, re.MULTILINE+re.DOTALL+re.IGNORECASE):
            return True
        else:
            return False
    elif service_name == 'Named':
        result = runCommand(['/bin/ps', 'aux'])
        if result and re.search(r'(named)', result, re.MULTILINE+re.DOTALL):
            return True
        else:
            return False

def checkIPMI():
    kernel = platform.uname()[2]
    location = '/lib/modules/' + kernel + '/kernel/drivers/char/ipmi/'
    if not os.path.exists(location):
        return None
    print "Loading IPMI modules...",
    for module in os.listdir(location):
        match = re.search("(ipmi_\w+)\.ko", module)
        runCommand(['/sbin/modprobe', match.groups()[0]])
    print "done!"
    time.sleep(.5)
    
    if os.path.exists('/usr/bin/ipmitool'):
        ipmi = runCommand(['/usr/bin/ipmitool', 'lan', 'print', '1'])
        if ipmi:
            mac = re.search(r'^MAC\s\w+\s+:\s([\d\w]{2}:[\d\w]{2}:[\d\w]{2}:[\d\w]{2}:[\d\w]{2}:[\d\w]{2})$', ipmi, re.MULTILINE+re.DOTALL)
            if mac:
                print "[+] IPMI is detected"
                return True
        else:
            print bcolors.FAIL + "[-] IPMI is not supported on this server." + bcolors.ENDC
            return False
    # ipmitools not installed
    else:
        print bcolors.FAIL + "[-] Error: ipmi tools is not installed! - please manually install ipmitool \
        \nCentOS: yum -y install OpenIPMI-tools \
        \nDebian: apt-get install ipmitool" + bcolors.ENDC
        return False

def checkRaidDrives(controller):
    if controller == "MegaRAID":
        mega_general = runCommand(['/usr/local/bin/megacli', '-ldinfo', '-lall', '-a0' ])
        mega_battery = runCommand(['/usr/local/bin/megacli', '-AdpBbuCmd', '-GetBbuStatus', '-a0'])
        
        if mega_general:
            raid_level = re.findall(r'^RAID\sLevel\s+:\s([\w\d\s,-]+)$', mega_general, re.MULTILINE+re.DOTALL)
            raid_size = re.findall(r'^Size\s+:\s([\d\.]+\s[\w]{2})', mega_general, re.MULTILINE+re.DOTALL)
            raid_state = re.findall(r'^State\s+:\s([\w]+)$', mega_general, re.MULTILINE+re.DOTALL)
            raid_strip_size = re.findall(r'^Strip\sSize\s+:\s([\d\w\s]+)$', mega_general, re.MULTILINE+re.DOTALL)
            if mega_battery:
                battery_state = re.findall(r'^Battery\sState\s+:\s([\w\d\s]+)$', mega_battery, re.MULTILINE+re.DOTALL)
                charging_status = re.findall(r'^\s+Charging\sStatus\s+:\s([\w\d\s]+)$', mega_battery, re.MULTILINE+re.DOTALL)
                charge_percent = re.findall(r'^\s+Relative\sState\sof\sCharge:\s([\w\s\d%]+)$', mega_battery, re.MULTILINE+re.DOTALL)
                cache_settings = re.findall(r'^Current\sCache\sPolicy:\s([\w\s,]+)$', mega_general, re.MULTILINE+re.DOTALL)
            
                if battery_state:
                    print "Battery State: %s" % battery_state[0].strip()
                    print "Charging Status: %s" % charging_status[0]
                    print "Charge Level: %s" % charge_percent[0]
                    wbb = runCommand(['/usr/local/bin/megacli', '-LDSetProp', 'WB', '-LALL', '-aALL']) # Enable BBU Write Cache
                    runCommand(['/usr/local/bin/megacli', '-LDSetProp', 'NoCachedBadBBU', '-LALL', '-aALL']) # Also disable write cache if BBU fails
                    print "Current Cache Policy: %s" % cache_settings[0]
            print "------------------------"

            # If Server contains a raid and a backup drive -- print info for both
            if len(raid_level) == 2:
                for i in range(2):
                    print "Raid Level: %s" % raid_level[i]
                    print "Size: %s" % raid_size[i]
                    print "State: %s" % raid_state[i]
                    print "Strip Size: %s" % raid_strip_size[i]
                    print "------------------------"
                  
            # If Server has 1 raid
            elif len(raid_level) == 1:
                print "Raid Level: %s" % raid_level[0]
                print "Size: %s" % raid_size[0]
                print "State: %s" % raid_state[0]
                print "Strip Size: %s" % raid_strip_size[0]
                print "------------------------" 

            #-------------------------------------------
            # Check individual drives in array 
            #-------------------------------------------
            # -pdlist -a0
            mega_drives = runCommand(['/usr/local/bin/megacli', '-pdlist', '-a0'])
            port_statuses = re.findall(r'^Firmware\sstate:\s([\w\s(),]+)$', mega_drives, re.MULTILINE+re.DOTALL)
            for port in range(len(port_statuses)):
                print "Port %d is %s" % (port, port_statuses[port])
            print "------------------------" 

            print ""
        
    elif controller == "3ware":
        # Add check in for /root/3ware/tw_cli binary -- Need to get correct path
        if os.path.exists('/usr/local/bin/3ware'):
            threeware = '/usr/local/bin/3ware'
        else:
            threeware = None
        if threeware:
            subprocess.call([threeware, '/c0', 'show'])

    elif controller == "Adaptec":
        adaptec_general = runCommand(['/usr/local/bin/arcconf', 'getconfig', '1'])
        if adaptec_general:
            controller_model = re.findall(r'^\s+Controller\sModel\s+:\s([\w\s\d]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            firmware_version = re.findall(r'^\s+Firmware\s+:\s([\w\d\.\-\s()]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_level = re.findall(r'^\s+RAID\slevel\s+:\s([\d\w]+)', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_size = re.findall(r'^\s+Size\s+: ([\d\s\w]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_state = re.findall(r'^\s+Status\sof\slogical\sdevice\s+:\s(\w+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_bbu_status = re.findall(r'^\s+Status\s+:\s(Charging|Optimal)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_bbu_capacity = re.findall(r'^\s+Capacity\sremaining\s+:\s(\d{1,3}\spercent)$', adaptec_general, re.MULTILINE+re.DOTALL)
            raid_bbu_time_Remaining = re.findall(r'^\s+Time\sremaining\s\(at\scurrent\sdraw\)\s+:\s([\d\s\w,]+)$', adaptec_general, re.MULTILINE+re.DOTALL) # use a .strip()
            raid_cache_setting = re.findall(r'^\s+Write-cache\ssetting\s+:\s([\w\s()-\/]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            
            # Check BBU
            if raid_bbu_status:
                print "BBU Status: %s" % raid_bbu_status[0]
                if raid_bbu_capacity:
                    print "Capacity Remaining: %s" % raid_bbu_capacity[0]
                if raid_bbu_time_Remaining:
                    print "Time Remaing (at current draw): %s" % raid_bbu_time_Remaining[0].strip()
                wbb = runCommand(['/usr/local/bin/arcconf', 'setcache', '1', 'logicaldrive', '0', 'wbb'])
                if wbb:
                    print "Write Back Cache has been enabled. Please re-run script to confirm."
                if raid_cache_setting:
                    print "Write-cache Setting: %s" % raid_cache_setting[0]
            print "------------------------"

            # Output if 1 raid
            if len(raid_level) == 1:
                print "Controller Model: %s" % controller_model[0]
                print "Firmware Version: %s" % firmware_version[0]
                print "Raid Level: %s" % raid_level[0]
                print "Raid Size: %s" % raid_size[0]
                print "Raid State: %s" % raid_state[0]
                print "------------------------"

            # Output if raid  with a backup drive
            elif len(raid_level) == 2:
                print "Controller Model: %s" % controller_model[0]
                print "Firmware Version: %s" % firmware_version[0]
                for i in range(2):
                    print "Raid Level: %s" % raid_level[i]
                    print "Raid Size: %s" % raid_size[i]
                    print "Raid State: %s" % raid_state[i]
                    print "------------------------"
    
            #-------------------------------------------
            # Check individual drives in Adaptec array 
            #-------------------------------------------
            device_state = re.findall(r'^\s+State\s+:\s([\w\s]+)$', adaptec_general, re.MULTILINE+re.DOTALL)
            for device in range(len(device_state)):
                print "Port %d is %s" % (device, device_state[device])
            print ""

# Main Method
def main():

    # Clear the shell buffer
    sys.stderr.write("\x1b[2J\x1b[H")

    #-------------------------------------------
    # Get CPU Info
    #-------------------------------------------
    cpu_info = openFile('/proc/cpuinfo')
    if not cpu_info:
        print bcolors.FAIL + "[-] Error: Could not open CPU information." + bcolors.ENDC
    
    else:
        print_cpu = parseCPU(cpu_info)
        if print_cpu:
            print "CPU: " + print_cpu + "\n"
        else:
            print "Could not find a valid cpu!"

    #-------------------------------------------
    # Get Ram Info
    #-------------------------------------------
    ram = runCommand(['/usr/bin/free', '-m'])
    print ram

    #-------------------------------------------
    # Check mount points
    #-------------------------------------------
    mount_points = runCommand(['/bin/df', '-h'])
    if mount_points:
        print mount_points
    else:
        print bcolors.FAIL + "[-] Error: Could not read mount points" + bcolors.ENDC + "\n"
        
    
    #-------------------------------------------
    # Verify Backups
    #-------------------------------------------
    
    if "/backup" not in mount_points:
        print bcolors.FAIL + "[-] Error: The backup drive is not mounted!" + bcolors.ENDC + "\n"
        backup_mounted = False
    else:
        backup_mounted = True

    #-------------------------------------------
    # Check cpanel backup config (/etc/cpbackup.conf)
    #-------------------------------------------
    if backup_mounted:
        checkBackupConfig()

    #-------------------------------------------
    # Check For Raid Card
    #-------------------------------------------
    raid_card = checkRaidCard()
    if not raid_card:
        print bcolors.FAIL + "This server does not have a raid card installed\n" + bcolors.ENDC
    else:
        print "Server Raid: %s\n" % raid_card
        checkRaidDrives(raid_card)
    
    #-------------------------------------------
    # Check OS Release
    #-------------------------------------------
    print checkOSRelease(),
    
    #-------------------------------------------
    # Check Kernel Version
    #-------------------------------------------
    print "Kernel: " + platform.uname()[2]
    print "Arch: " + platform.uname()[4] + "\n"

    #-------------------------------------------
    # Check Ethspeed
    #-------------------------------------------
    eth = runCommand(['/sbin/ethtool', 'eth0']);
    default_eth = ''
    if eth:
        eth_speed = parseEthSpeed(eth)
        if eth_speed:
            print "Port Speed: " + eth_speed
            default_eth = 'eth0'
        # Double check for eth1 incase eth0 is not present
        elif eth_speed is None:
            eth = runCommand(['/sbin/ethtool', 'eth1'])
            isEth1 = True
            if eth:
                eth_speed = parseEthSpeed(eth)
                if eth_speed:
                    print "Port Speed: " + eth_speed
                    default_eth = 'eth1'
    else:
        print bcolors.FAIL + "[-] Could not determine eth speed! Check to see if eth0 or eth1 exist!\n\
        You may need to remove 70-persistant-net.rules from /etc/udev/rules.d\n\
        && reboot the server to see correct eth devices" + bcolors.ENDC

    print "Default Ethernet Device: %s" % default_eth
            
    #-------------------------------------------
    # List Eth MAC's
    #-------------------------------------------
    eth_macs = runCommand(['/sbin/ifconfig', default_eth])
    if eth_macs:
        new_eth_macs = parseEthMacs(eth_macs, default_eth)
        if new_eth_macs:
            print "MAC Address: " + new_eth_macs
        else:
            print bcolors.FAIL + "[-] Error: Could not read eth devices" + bcolors.ENDC
    print "\n",
    
    #-------------------------------------------
    # Check services (Axon, Apache, Mysql, Named)
    #-------------------------------------------
    for service in ['Axon', 'Apache', 'MySQL', 'Named']:
        result = checkService(service)
        if result:
            print service + " is running"
        else:
            print bcolors.FAIL + service + " IS DOWN!" + bcolors.ENDC
    print "\n",
    
    #-------------------------------------------
    # IP Ping out Results
    #-------------------------------------------
    system_ips = getIPS()
    if system_ips:
        for ip in system_ips:
            if ip == '127.0.0.1':
                continue
            else:
                result = testPing(ip)
                if result:
                    print "IP " + ip + " is up"
                else:
                    print bcolors.FAIL + "IP " + ip + " is DOWN!" + bcolors.ENDC
    print ""

    # ------------------------------------------
    # IPMI
    #-------------------------------------------
    checkIPMI()

if __name__ == "__main__":
    main()